# LINMOBapps

## LINux on MOBile Apps

### DEPRECATED

This project has been deprecated, all action is now happening over at [https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io). The repo is kept as is for historic purposes.

### History
This project was served at [https://linmobapps.frama.io/](https://linmobapps.frama.io/) and has been forked from the great [https://mglapps.frama.io/](https://mglapps.frama.io) list always with the goal of creating something static site generator based with the name "LinuxPhoneApps.org". On March 28th, 2022, after more than one year of interim LINMOBapps existence, [LinuxPhoneApps.org](https://linuxphoneapps.org) was finally launched - but this is still the place where the .csv files that contain all the data are being stored and edited.

### Structure
Files:
* [complete.csv](complete.csv): Complete list of apps, to maintain remerge-ability to MGLapps, no longer maintained,
* [apps.csv](apps.csv)__: Main app list (subset of complete.csv), to be edited directly. _Add your apps here ([instructions](https://linuxphoneapps.org/docs/contributing/edit-csv-in-libreoffice/))!_
* [apps-to-be-added.md](apps-to-be-added.md)__: Apps waiting to be added.
* [games.csv](games.csv)__: Main games list (subset of complete.csv), to be edited directly.
* [archive.csv](archive.csv): Retired apps (subset of complete.csv).
* [other games.csv](other apps.csv): Further games which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the main list

### Licensing
Just like its origin [MGLApps](https://mglapps.frama.io), LINux on MOBile Apps is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/) . For more licensing information, see [LICENSE](LICENSE)

### Contributing
See https://linuxphoneapps.org/docs/contributing and the [linuxphoneapps.frama.io README.md](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/README.md).


